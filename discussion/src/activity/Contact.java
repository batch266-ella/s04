package activity;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    @Override
    public String toString() {
        return ("{name: " + this.name + ", contact: " + this.contactNumber + ", address: " + this.address + "}");
    }

    public boolean isContactEmpty() {
        return (name == null && contactNumber == null && address == null) ? true : false;
    }

    // Default constructor
    public Contact() {}
    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
