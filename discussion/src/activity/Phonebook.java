package activity;
import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Default Constructor
    public Phonebook() {}
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void viewContacts() {
        int emptyContactCount = 0;
        String message = "";
        if (contacts.size() == 0) {
            System.out.println("The phonebook is empty.");
        } else {
            System.out.println("The contacts in the phonebook are:");
            for (int i = 0; i < contacts.size(); i++) {
                if (contacts.get(i).isContactEmpty()) {
                    emptyContactCount++;
                } else {
                    message += contacts.get(i) + "\n";
                }
            }

            if (emptyContactCount == contacts.size()) {
                System.out.println("The phonebook is empty.");
            } else {
                System.out.println(message);
            }
        }
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }
}
