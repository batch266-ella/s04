package activity;

public class Main {
    public static void main(String[] args) {
        /* Non-empty example */
        System.out.println("Case 1");
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Imman", "09876543210", "Cavite");
        Contact contact2 = new Contact("Eula", "123", "Mondstadt");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.viewContacts();

        /* Empty example */
        System.out.println("Case 2");
        Phonebook emptyPhoneBook = new Phonebook();

        emptyPhoneBook.viewContacts();
        System.out.println();

        /* Phonebook has Contacts but all contacts are empty */
        System.out.println("Case 3");
        Phonebook phonebookWithEmptyContacts = new Phonebook();

        Contact contact3 = new Contact();
        Contact contact4 = new Contact();

        emptyPhoneBook.viewContacts();
        
    }
}
