package com.zuitt.example;

public class Animal {
    private String name;
    private String color;

    // Default Constructor
    public Animal() {}
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    /* methods */
    public void call() {
        System.out.println("Hi, my name is " + this.name);
    }
}
