package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");

        myPet.speak();

        System.out.println(myPet.getName() + " " + myPet.getBreed() + " " + myPet.getColor());

        Dog myDreamPet = new Dog("Pancake", "Brown and White", "BullShihtz");
        myDreamPet.speak();
        System.out.println(myDreamPet.getName() + " " + myDreamPet.getBreed() + " " + myDreamPet.getColor());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);

        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());
        System.out.println("Car driver: " + myCar.getDriverName());

        // Abstraction
        /* is a process where all the logic and complexity are hidden from the users */

        Person child = new Person();
        child.sleep();
        child.run();

        // Polymorphism
        /* Derived from the Greek word "poly" which means many and "morph" which means forms */
        StaticPoly myAddition = new StaticPoly();

        System.out.println(myAddition.addition(5, 6));
        System.out.println(myAddition.addition(5, 6, 10));
        System.out.println(myAddition.addition(5.5, 6.6));

    }
}
